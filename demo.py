#!/usr/bin/env python3
import torch
import cv2 as cv
import time
from motion_simulator import MotionSimulator
from model import RexNet as Net

if __name__ == "__main__":
    print("Hello There!")

    save_video = True
    if save_video:
        out_size = (192,192)
        fourcc = cv.VideoWriter_fourcc(*"MJPG")
        video_out = cv.VideoWriter("output.avi",fourcc,15,out_size)

    try:
        net = torch.load("weights.pth")
        net.eval()
        with torch.no_grad():
            cv.namedWindow("frame",0)
            # cv.namedWindow("conf",0)
            seed = 0
            while True:
                seed += 1
                sim = MotionSimulator(seed)
                last_frame,(x,y) = sim.next()
                last_frame_tensor = torch.from_numpy(last_frame).unsqueeze(dim=0).unsqueeze(dim=0).cuda().float() / 255.0

                for _ in range(60):

                    frame,(x,y) = sim.next()

                    frame_tensor = torch.from_numpy(frame).unsqueeze(dim=0).unsqueeze(dim=0).cuda().float() / 255.0

                    output = net(frame_tensor,last_frame_tensor)

                    last_frame_tensor = frame_tensor

                    conf = output[0,0,:,:].cpu().numpy()
                    x_pred = output[0,1,:,:].cpu().numpy()
                    y_pred = output[0,2,:,:].cpu().numpy()
                    # cv.imshow("conf",conf)

                    grid_h,grid_w = conf.shape

                    frame_bgr = cv.cvtColor(frame,cv.COLOR_GRAY2RGB)
                    frame_bgr = cv.resize(frame_bgr,(0,0),fx=2,fy=2)
                    frame_h,frame_w,_ = frame_bgr.shape
                    for xi in range(grid_w):
                        for yi in range(grid_h):

                            cell_conf = float(conf[yi,xi])

                            if cell_conf > 0.5:
                                x = int( (xi + float(x_pred[yi,xi]) ) / grid_w * frame_w)
                                y = int( (yi + float(y_pred[yi,xi]) ) / grid_h * frame_h)

                                # cv.circle(frame_bgr,(x,y),5,(255,0,0),0,lineType=cv.LINE_AA)
                                color = (238,178,35)
                                b=10
                                cv.line(frame_bgr,(0,y),(x-b,y),color)
                                cv.line(frame_bgr,(x+b,y),(frame_w,y),color)
                                cv.line(frame_bgr,(x,0),(x,y-b),color)
                                cv.line(frame_bgr,(x,y+b),(x,frame_h),color)

                    print(frame_bgr.shape)
                    cv.imshow("frame",frame_bgr)
                    if save_video:
                        video_out.write(frame_bgr)
                    cv.waitKey(int(1/15*1000))
                cv.waitKey(100)
    except KeyboardInterrupt:
        pass
    finally:
        if save_video:
            video_out.release()
