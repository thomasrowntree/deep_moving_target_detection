#!/usr/bin/env python3

import torch
import torch.nn as nn
import torch.nn.functional as F


class ConvBlock(nn.Module):
    def __init__(self,in_channels, out_channels, kernel_size, stride=1, padding=0, dilation=1, groups=1, bias=True):
        super(ConvBlock, self).__init__()
        self.conv = nn.Conv2d( in_channels, out_channels, kernel_size, stride, padding, dilation, groups, bias)
        self.bn = nn.BatchNorm2d( out_channels)
        self.relu = nn.LeakyReLU(0.1, inplace=True)

    def forward(self, x):
        out = self.conv(x)
        out = self.bn(out)
        out = self.relu(out)

        return out

class BottleNeck(nn.Module):
    def __init__(self,in_channels,out_channels):
        super(BottleNeck, self).__init__()

        mid_channels = out_channels // 2

        self.conv1 = ConvBlock(  in_channels, out_channels, kernel_size=3, stride=1, padding=1, bias=False)
        self.conv2 = ConvBlock( out_channels, mid_channels, kernel_size=1, stride=1, padding=0, bias=False)
        self.conv3 = ConvBlock( mid_channels, out_channels, kernel_size=3, stride=1, padding=1, bias=False)


    def forward(self, x):
        out = self.conv1(x)
        out = self.conv2(out) #1x1 bottle neck
        out = self.conv3(out)

        return out

class RexNet(nn.Module):
    def __init__(self):
        super(RexNet, self).__init__()


        self.down_conv1 = ConvBlock(  2, 32, kernel_size=3, stride=1, padding=1, bias=False)
        self.down_conv2 = ConvBlock(  32, 64, kernel_size=3, stride=1, padding=1, bias=False)
        self.down_conv3 = BottleNeck( 64, 128)
        self.down_conv4 = BottleNeck( 128, 128)
        self.down_conv5 = BottleNeck( 128, 128)
        self.down_conv6 = BottleNeck( 128, 128)

        self.up_conv1 = ConvBlock(  128, 32, kernel_size=1, stride=1, padding=0, bias=False)
        self.up_conv2 = BottleNeck( 32+128, 32)
        self.up_conv3 = BottleNeck( 32+128, 32)

        self.out_conv1 = BottleNeck( 32+128, 16)
        self.out_conv2 = nn.Conv2d(  16, 3, kernel_size=1, stride=1, padding=0, bias=False)


        self.pool = nn.MaxPool2d(2, 2)
        self.avgpool = nn.AdaptiveAvgPool2d((1,1))
        self.sigmoid = nn.Sigmoid()

    def forward(self, frame, last_frame):

        out = torch.cat((frame,last_frame),dim=1)

        out = self.down_conv1(out) #96
        out = self.pool(out) #48

        out = self.down_conv2(out) #48
        out = self.pool(out) #24

        out = self.down_conv3(out) #24
        out = self.pool(out) #12

        out = self.down_conv4(out) #12
        skip3 = out
        out = self.pool(out) #6

        out = self.down_conv5(out) #6
        skip2 = out
        out = self.pool(out) #3

        out = self.down_conv6(out) #3
        skip1 = out
        out = self.avgpool(out) #1

        out = self.up_conv1(out) #1
        out = F.interpolate( out, skip1.shape[-2:], mode="bilinear", align_corners=False) #3
        out = torch.cat((skip1,out),dim=1) #3

        out = self.up_conv2(out) #3
        out = F.interpolate( out, skip2.shape[-2:], mode="bilinear", align_corners=False) #6
        out = torch.cat((skip2,out),dim=1) #6

        out = self.up_conv3(out) #6
        out = F.interpolate( out, skip3.shape[-2:], mode="bilinear", align_corners=False) #12
        out = torch.cat((skip3,out),dim=1) #12

        out = self.out_conv1(out)
        out = self.out_conv2(out)

        out = self.sigmoid(out)
        return out

if __name__ == "__main__":
    print("Hello There!")
    from torchsummary import summary
    net = RexNet()
    net.cuda()
    summary(net,[(1,96,96),(1,96,96)])
    for n,t in net.named_parameters():
        print(n)
