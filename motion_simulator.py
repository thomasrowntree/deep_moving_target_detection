#!/usr/bin/env python3

import math
import numpy as np
import random
import cv2 as cv

class MotionSimulator():
    def __init__(self, seed, img_size=(96,96), speed_range=(2.0,6.0), particle_range=(10,20)):

        self.img_size = img_size
        h,w = self.img_size

        #seed the random generators
        r = random.Random(seed)
        np_r = np.random.RandomState(seed)

        #pick a random number of particles within the range provided
        self.particle_count = r.randint(*particle_range)

        #randomly choose the direction of the target and background particles
        background_direction = r.random()*360.0

        #Ensure the target direction is outside 90 degrees of the background direction
        target_direction =background_direction + 90. + r.random()*180.0

        #convert directions to radians
        background_direction = math.radians(background_direction)
        target_direction = math.radians(target_direction)

        #randomly choose background and target speeds
        background_speed = r.uniform(*speed_range)
        target_speed = r.uniform(*speed_range)

        #create nx2 matrix to hold particle speeds
        self.velocities = np.zeros((self.particle_count, 2)) #vx,vy

        #The first row contains the velocity of the target
        self.velocities[0,0] = target_speed * math.cos(target_direction) #target x speed
        self.velocities[0,1] = target_speed * math.sin(target_direction) #target y speed

        #Every row except the first has the velocity of the background
        self.velocities[1:,0] = background_speed * math.cos(background_direction) #background x speed
        self.velocities[1:,1] = background_speed * math.sin(background_direction) #background y speed

        #Randomly choose starting positions of the particles
        self.positions = np_r.random_sample((self.particle_count, 2)) #x,y
        self.positions[:,0] *= w
        self.positions[:,1] *= h


    def next(self):

        h,w = self.img_size

        #Take update step by adding velocities to positions
        self.positions += self.velocities

        #wrap the particles around the edges of the image
        self.positions[:,0] = np.remainder(self.positions[:,0],w)
        self.positions[:,1] = np.remainder(self.positions[:,1],h)

        #Open cv can't do anti aliasing so draw it bigger then down sample

        #each pyramid step doubles the image size
        pyramid_scale = 2
        scale = 2**pyramid_scale

        #create empty frame
        frame = np.zeros((h*scale,w*scale),dtype="uint8")

        #scale up the positions then convert to ints for drawing
        positions_floor = np.floor(self.positions*scale).astype(np.int)

        #for each point draw a circle at its location
        for point_i in range(self.particle_count):
            x = positions_floor[point_i,0]
            y = positions_floor[point_i,1]
            cv.circle(frame,(x,y),scale,(255),-1,lineType=cv.LINE_AA)

        #for each pyramid_scale repeatedly resize the image by half
        for i in range(pyramid_scale):
            frame = cv.resize(frame,None,fx=0.5,fy=0.5)

        #Calculate the normalized target positions
        target_x = self.positions[0,0] / w
        target_y = self.positions[0,1] / h


        return (frame,(target_x,target_y))




if __name__ == "__main__":
    print("Hello There!")

    import cv2 as cv
    import time
    cv.namedWindow("frame",0)
    seed = 0
    while True:
        seed += 1
        sim = MotionSimulator(1)
        for _ in range(2):

            frame,(x,y) = sim.next()
            # cv.circle(frame,(int(x*96),int(y*96)),5,(255),0,lineType=cv.LINE_AA)
            cv.imshow("frame",frame)
            cv.waitKey(500)
        cv.waitKey(2000)
