#!/usr/bin/env python3
import torch
from torch.utils.data import Dataset
import random
from motion_simulator import MotionSimulator

class MotionDataset(Dataset):
    def __init__(self,dataset_len,dataset_seed,img_size=(96,96),speed_range=(2.0,6.0),particle_range=(10,20)):
        self.dataset_len = dataset_len
        self.dataset_seed = dataset_seed
        self.img_size = img_size
        self.speed_range = speed_range
        self.particle_range = particle_range
        self.max_targets = 8


    def __len__(self):
        return self.dataset_len

    def __getitem__(self, index):
        random.seed(self.dataset_seed)

        #seeding with index enusures that the index always gets the same sample
        sample_seed = int(random.random()*999999999 + index)

        #Instanciate simulator
        sim = MotionSimulator(sample_seed, self.img_size, self.speed_range, self.particle_range)

        #get to frames from the simulator
        frame0,(x0,y0) = sim.next()
        frame1,(x1,y1) = sim.next()

        #turn the numpy frames into tensors shape=(c,h,w)
        frame0_tensor = torch.from_numpy(frame0).unsqueeze(0).float()/255.0
        frame1_tensor = torch.from_numpy(frame1).unsqueeze(0).float()/255.0

        #Turn target into tensor shape=(n,3)
        target_tensor = torch.zeros((self.max_targets,3)).float()

        #there is only one target at the moment so just put it in
        target_tensor[0,0] = 1.
        target_tensor[0,1] = x1
        target_tensor[0,2] = y1


        #Create sample dict
        sample = {"frame": frame1_tensor, "last_frame": frame0_tensor, "target": target_tensor}
        return sample

if __name__ == "__main__":
    import time
    import cv2

    md = MotionDataset(100,2,img_size=(96,96))

    cv2.namedWindow("frame",0)
    for i in range(len(md)):

        data = md[i]

        frame = (data["frame"].squeeze(0).numpy()*205).astype ("uint8")
        frame += (data["last_frame"].squeeze(0).numpy()*50).astype ("uint8")
        frame = cv2.cvtColor(frame,cv2.COLOR_GRAY2RGB)

        targets = data["target"]

        for j in range(targets.shape[0]):
            h,w,c = frame.shape
            x,y = targets[j]
            cv2.circle(frame,(int(x*w),int(y*h)),1,(0,255,0),0,lineType=cv2.LINE_AA)

        cv2.imshow("frame",frame)
        cv2.waitKey(500)
        cv2.waitKey(1000)
