#!/usr/bin/env python3
import math
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torchsummary import summary
from synthetic_motion_dataset import MotionDataset

from model import RexNet as Net

class DetectionLoss(nn.Module):
    def __init__(self):
        super(DetectionLoss, self).__init__()


    def forward(self, output, target):
        b,c,h,w = output.shape
        b,n,_  = target.shape


        conf_pred = output[:,0,:,:]
        x_pred = output[:,1,:,:]
        y_pred = output[:,2,:,:]


        conf_target = torch.zeros(conf_pred.shape)
        x_target = torch.zeros(x_pred.shape)
        y_target = torch.zeros(y_pred.shape)
        for i_batch in range(b):
            for i_target in range(n):
                obj,x,y = target[i_batch, i_target]

                if obj > 0:
                    i_x = int(x*w)
                    i_y = int(y*h)
                    conf_target[i_batch,i_y,i_x] = 1.0

                    x_target[i_batch,i_y,i_x] = x*w - math.floor(x*w)
                    y_target[i_batch,i_y,i_x] = y*h - math.floor(y*h)


        conf_target = conf_target.cuda()
        x_target = x_target.cuda()
        y_target = y_target.cuda()

        conf_loss = nn.MSELoss()(conf_pred,conf_target)
        #use the conf target as a mask so that only cells with the target have the regression updated
        x_loss = nn.MSELoss()(x_pred * conf_target, x_target)
        y_loss = nn.MSELoss()(y_pred * conf_target, y_target)

        loss = conf_loss + x_loss + y_loss
        return loss


if __name__ == "__main__":
    print("Hello There!")

    save_interval = 1000
    train_dataset = MotionDataset(dataset_len=1000000,dataset_seed=1,img_size=(96,96),speed_range=(2.0,6.0),particle_range=(10,20))
    train_loader = torch.utils.data.DataLoader(train_dataset,batch_size=64)

    net = Net()
    net.train()
    net.cuda()

    criterion = DetectionLoss()

    optimiser = optim.Adam(net.parameters())

    for i_batch, data in enumerate( train_loader ):
        frame = data["frame"].cuda()
        last_frame = data["last_frame"].cuda()
        target = data["target"]

        optimiser.zero_grad()
        output = net(frame,last_frame)
        loss = criterion(output,target)

        loss.backward()
        optimiser.step()

        print("Batch: %i/%i, Loss: %0.6f" % (i_batch,len(train_loader), float(loss.cpu()) ) )

        if i_batch % save_interval == (save_interval-1):
            print("save")
            torch.save(net,"weights.pth")
    torch.save(net,"weights.pth")
